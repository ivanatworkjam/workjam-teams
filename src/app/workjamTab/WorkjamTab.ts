import { PreventIframe } from "express-msteams-host";

/**
 * Used as place holder for the decorators
 */
@PreventIframe("/workjamTab/index.html")
@PreventIframe("/workjamTab/config.html")
@PreventIframe("/workjamTab/remove.html")
export class WorkjamTab {
}
